<?php

use PiPHP\GPIO\Pin\PinInterface;

require_once 'vendor/autoload.php';

$gpio = new \PiPHP\GPIO\GPIO();
$pinNumber = 18;
$pin = $gpio->getOutputPin($pinNumber);
$value = $pin->getValue();
echo "pin $pinNumber has current value $value";
$pin->setValue($value == PinInterface::VALUE_HIGH ? PinInterface::VALUE_LOW : PinInterface::VALUE_HIGH);
$value = $pin->getValue();
echo " and we changed it to $value";